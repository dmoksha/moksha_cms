DmLms::Engine.routes.draw do
  scope ":locale" do
    post '/lesson_pages/ajax_add_comment',          controller: 'lesson_pages', action: :ajax_add_comment, as: :lesson_page_add_comment
    delete '/lesson_pages/ajax_delete_comment/:id', controller: 'lesson_pages', action: :ajax_delete_comment, as: :lesson_page_delete_comment
    namespace :admin do
      get '/dashboard/widget_lesson_comments(/:comment_day)', controller: 'dashboard', action: :widget_lesson_comments, as: :widget_lesson_comments
      scope 'lms' do
        # --- simplifying nested resources, from http://weblog.jamisbuck.org/2007/2/5/nesting-resources
        resources :courses do
          resources :lessons do
            resources :lesson_pages
            resources :teachings
          end
        end
        post '/courses/sort', controller: 'courses', action: :sort, as: :course_sort
        post '/lessons/sort', controller: 'lessons', action: :sort, as: :lesson_sort
        post '/lesson_pages/sort', controller: 'lesson_pages', action: :sort, as: :lesson_page_sort
      end
    end

    scope 'learn' do
      get    '/courses',                                   controller: 'courses', action: :index, as: :course_index
      get    '/:course_slug',                              controller: 'courses', action: :show, as: :course_show
      get    '/:course_slug/:lesson_slug',                 controller: 'lessons', action: :show, as: :lesson_show
      get    '/:course_slug/:lesson_slug/:content_slug',   controller: 'lesson_pages', action: :show, as: :lesson_page_show
      post   '/:course_slug/:lesson_slug/:content_slug/ajax_add_comment', controller: 'lesson_pages', action: :ajax_add_comment, as: :course_lesson_lesson_page_ajax_add_comment
      delete '/:course_slug/:lesson_slug/:content_slug/ajax_delete_comment/:id', controller: 'lesson_pages', action: :ajax_delete_comment, as: :course_lesson_lesson_page_ajax_delete_comment_comment
    end
  end
end
