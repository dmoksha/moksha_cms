module DmEvent::Workshops
  class DuplicateService
    include DmCore::ServiceSupport

    #------------------------------------------------------------------------------
    def initialize(workshop)
      @workshop = workshop
    end

    #------------------------------------------------------------------------------
    def call
      new_workshop = @workshop.amoeba_dup

      return false unless new_workshop.save

      @workshop.system_emails.each do |email|
        new_email = email.amoeba_dup
        new_email.emailable_id = new_workshop.id
        new_email.save
      end

      new_workshop
    end
  end
end
