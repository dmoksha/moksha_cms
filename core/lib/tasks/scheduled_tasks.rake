# frozen_string_literal: true

namespace :scheduled do
  # any users that haven't confirmed within the confirmation time limit
  # should be removed. This helps reduce spam accounts that make it
  # through recaptcha, etc.
  desc 'Remove unconfirmed users'
  task remove_unconfirmed_users: :environment do
    puts "--- Removing unconfirmed users at #{Time.now}"
    users = User.unscoped.confirmation_expired

    removed = users.destroy_all
    puts "      removed #{removed.count} users"
  end

  # any users that have confirmed but have not signed in for the first
  # time within a time limit of account creation will be removed.
  # This helps reduce spam accounts that make it through recaptcha, etc.
  desc 'Remove never signed in users'
  task remove_never_signed_in_users: :environment do
    puts "--- Removing never signed in users at #{Time.now}"
    users = User.unscoped.never_signed_in

    removed = users.destroy_all
    puts "      removed #{removed.count} users"
  end

  # any users that have the same first and last name is likely spam.
  # new validations should eliminate this, but attempt cleanup anyway.
  # time within a time limit of account creation will be removed.
  desc 'Remove users with the same first name == last name'
  task remove_users_same_first_last_name: :environment do
    puts "--- Removing users with the same first name == last name at #{Time.now}"
    users = User.unscoped.joins(:user_profile).where('user_profiles.first_name = user_profiles.last_name')

    removed = users.destroy_all
    puts "      removed #{removed.count} users"
  end
end
