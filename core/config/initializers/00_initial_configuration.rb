# frozen_string_literal: true

# Load configuration values
#------------------------------------------------------------------------------
APPLICATION_CONFIG_FILE = 'application.yml'

begin
  YAML.load_file(config_file = Rails.root.join('config', APPLICATION_CONFIG_FILE), aliases: true)[Rails.env].each do |k, v|
    Rails.application.config.send("#{k}=", v)
  end

  Rails.application.config.action_mailer.default_url_options = { host: Rails.application.config.base_domain }
  Rails.application.config.hosts += Rails.application.config.development_hosts if Rails.env.development?
  APPLICATION_SESSION_STORE_KEY = Rails.application.config.application_session_store_key
rescue StandardError => e
  Rails.logger.debug "=== Error initializing: Issue processing config file #{config_file}.\n\nError is: #{e}"
end
