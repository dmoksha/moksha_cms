# frozen_string_literal: true

# https://github.com/rails/sprockets-rails/issues/49

# gem 'non-stupid-digest-assets' (https://github.com/alexspeller/non-stupid-digest-assets)
# ensures that a non-digest version of the asset is written
# allows some themes to easily reference font files, etc
# The gem is no longer maintained.  But we still need this ability.
# So pulled code below from
# https://github.com/alexspeller/non-stupid-digest-assets/issues/48
# TODO: We need to find a way to remove the need for this

module NonStupidDigestAssets
  mattr_accessor :whitelist
  @@whitelist = [] # rubocop:disable Style/ClassVars

  class << self
    def files(files)
      return files if whitelist.empty?

      whitelisted_files(files)
    end

    private

    def whitelisted_files(files)
      files.select do |_file, info|
        whitelist.any? do |item|
          case item
          when Regexp
            info['logical_path'] =~ item
          else
            info['logical_path'] == item
          end
        end
      end
    end
  end
end

module NonDigest
  def compile(*args)
    super(*args)

    NonStupidDigestAssets.files(files).each do |(digest_path, info)|
      full_digest_path = File.join dir, digest_path
      full_digest_gz_path = "#{full_digest_path}.gz"
      full_non_digest_path = File.join dir, info['logical_path']
      full_non_digest_gz_path = "#{full_non_digest_path}.gz"

      if File.exist? full_digest_path
        # logger.info "Writing #{full_non_digest_path}"
        FileUtils.rm full_non_digest_path if File.exist? full_non_digest_path
        FileUtils.cp full_digest_path, full_non_digest_path
      else
        logger.warn "Could not find: #{full_digest_path}"
      end
      if File.exist? full_digest_gz_path
        # logger.info "Writing #{full_non_digest_gz_path}"
        FileUtils.rm full_non_digest_gz_path if File.exist? full_non_digest_gz_path
        FileUtils.cp full_digest_gz_path, full_non_digest_gz_path
      else
        logger.warn "Could not find: #{full_digest_gz_path}"
      end
    end
  end
end

module Sprockets
  class Manifest
    prepend NonDigest
  end
end
