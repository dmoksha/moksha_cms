# frozen_string_literal: true

# common Devise confirmation controller.  The main app can add methods
# to this class by "Reopening Existing Classes Using ActiveSupport::Concern",
# https://edgeguides.rubyonrails.org/engines.html#overriding-models-and-controllers
#------------------------------------------------------------------------------
module DmCore
  class ConfirmationsController < Devise::RegistrationsController
    include DmCore::Concerns::ConfirmationsController
  end
end
