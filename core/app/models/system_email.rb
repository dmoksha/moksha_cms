class SystemEmail < ApplicationRecord
  self.table_name = 'core_system_emails'

  belongs_to :emailable, polymorphic: true, optional: true

  default_scope { where(account_id: Account.current&.id) }

  # globalize
  translates          :subject, :body, fallbacks_for_empty_translations: true
  globalize_accessors locales: I18n.available_locales

  amoeba do
    enable
    nullify :emailable_id
  end
end
