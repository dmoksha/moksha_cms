# MokshaCMS

[![Gem Version](https://badge.fury.io/rb/moksha_cms.svg)](http://badge.fury.io/rb/moksha_cms)
[![Build Status](https://gitlab.com/moksha_cms/moksha_cms/badges/7-0-ruby-3-stable/pipeline.svg)](https://gitlab.com/moksha_cms/moksha_cms/-/commits/7-0-ruby-3-stable)

_The MokshaCMS collection of gems provides an integrated system of services for content, event, forum, learning, and newsletter management.  It supports sites with multiple languages and multiple distinct sites per installation.  Administration is built in.  Additional services/engines can be written to provide additional functionality._

- core foundation (core)
- content management (cms)
- event management (event)
- forum management (forum)
- newsletter management (newsletter)

## Installation

- For Rails 7.0 and Ruby 3, use

  `gem 'moksha_cms', git: 'https://github.com/digitalmoksha/moksha_cms.git',  branch: '7-0-ruby-3-stable'`

- For Rails 7.0 and Ruby 2.7, use

  `gem 'moksha_cms', git: 'https://github.com/digitalmoksha/moksha_cms.git',  branch: '7-0-stable'`

- For Rails 6.1, target `branch: '6-1-stable'`
- For Rails 6.0, target `branch: '6-0-stable'`
- For Rails 5, target `branch: '5-0-stable'`
- For Rails 4.2, target `branch: '4-2-stable'`

After running `bundle install`, run the following commands to install the migrations:

```
rake dm_core:install:migrations
rake dm_cms:install:migrations
rake dm_event:install:migrations
rake dm_forum:install:migrations
rake dm_newsletter:install:migrations
```

_Installation instructions are still a work in progress_

## Demo Application

You can grab the [Moksha CMS Demo](https://github.com/digitalmoksha/moksha_cms_demo) for an example of a full application.
